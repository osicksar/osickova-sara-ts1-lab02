package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class OsicksarTest {

    @Test
    public void factorialTest() {

        Osicksar osicksar = new Osicksar();
        int result1 = (int) osicksar.factorial (5);
        int result2 = (int) osicksar.factorial (1);
        Assertions.assertEquals(120,result1);
        Assertions.assertEquals(1,result2);
    }

    @Test
    public void factorial_nIs5_return120() {
        //Arrange
        Osicksar osicksar = new Osicksar();
        int n = 5;
        int expected = 120;

        //Act
        int actual = (int) osicksar.factorial(n);

        //Assert
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void factorial_nIsMinus1_return0() {
        //Arrange
        Osicksar osicksar = new Osicksar();
        int n = -1;
        int expected = 0;

        //Act
        int actual = (int) osicksar.factorial(n);

        //Assert
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void factorial_nIs0_return1() {
        //Arrange
        Osicksar osicksar = new Osicksar();
        int n = 0;
        int expected = 1;

        //Act
        int actual = (int) osicksar.factorial(n);

        //Assert
        Assertions.assertEquals(expected, actual);
    }
}
