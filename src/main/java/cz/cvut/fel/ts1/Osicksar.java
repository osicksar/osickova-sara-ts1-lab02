package cz.cvut.fel.ts1;

public class Osicksar {
	public long factorial(int n) {
		if (n > 0) {
			int result = 1;
			for (int i = 0; i < n; i++) {
				result = result * (n - i);
			}
			return result;
		}
		if (n == 0) {
			return 1;
		} else {
			return 0;
		}
	}
}
